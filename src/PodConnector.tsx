import {
  ILoginInputOptions,
  ISessionInfo,
  Session,
  getDefaultSession,
} from "@inrupt/solid-client-authn-browser"
import classNames from "classnames"
import { Formik } from "formik"
import React, {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react"
import Helmet from "react-helmet"

import styles from "./PodConnector.module.css"

interface PodConnectorProps {
  children: JSX.Element
  auto?: boolean
  name?: string
  logo?: JSX.Element
  lead?: JSX.Element
  loadingIndicator?: JSX.Element
  recommendedLogins?: string[]
  loginOptions?: Omit<ILoginInputOptions, "oidcIssuer">
}

interface PodConnectorContext {
  connect(): void
  abort(): void
  restoreState(): { from?: string; clearState(): void }
  session?: Session
}

const PodConnectorContext = React.createContext<PodConnectorContext>({
  connect: () => {},
  abort: () => {},
  restoreState: () => ({ clearState: () => {} }),
})

export const useConnect = () => {
  const { connect, ...rest } = useContext(PodConnectorContext)
  connect()
  return rest
}

export const useRestoreState = () => {
  const { restoreState } = useContext(PodConnectorContext)
  return restoreState()
}

export const PodConnector: React.FC<PodConnectorProps> = ({
  children,
  loadingIndicator,
  auto = true,
  name,
  lead,
  loginOptions,
  logo = (
    <img
      src="https://solidproject.org/assets/img/solid-emblem.svg"
      alt="Logo"
    />
  ),
  recommendedLogins = [
    "https://login.inrupt.com",
    "https://solidcommunity.net",
  ],
}) => {
  const [prevIdps, setPrevIdps] = useState<string[]>(
    JSON.parse(localStorage.getItem("prevIdps") ?? "[]")
  )
  const [newLogin, setNewLogin] = useState(
    Boolean(localStorage.getItem("newLogin"))
  )
  const [_clearInitialLoad, setClearInitialLoad] =
    useState<ReturnType<typeof setTimeout>>()

  const [activeWebId, setActiveWebId] = useState<string>()
  const [showRememberPromptFor, setShowRememberPromptFor] = useState<string>()

  const [manualTrigger, setManualTrigger] = useState(false)
  const [invalidIDP, setInvalidIDP] = useState(false)
  const [loading, setLoading] = useState(true)
  const [sessionExpired, setSessionExpired] = useState(false)
  const [sessionResponded, setSessionResponded] = useState(false)

  const session = getDefaultSession()

  useEffect(() => {
    if (
      loginOptions?.redirectUrl &&
      (auto || manualTrigger) &&
      location.pathname !== new URL(loginOptions?.redirectUrl).pathname
    ) {
      const params = new URLSearchParams(location.search)
      params.delete("code")
      const fromParams = params.size > 0 ? "?" + params : ""
      const fromHash = location.hash ? "#" + location.hash : ""
      const from = `${location.pathname}${fromHash}${fromParams}`
      localStorage.setItem("loginFrom", from)
    }

    session.onLogout(() => {
      console.debug("Logged out...")
      setSessionResponded(true)
      setLoading(false)
      setActiveWebId(undefined)
    })

    session.onSessionExpiration(() => {
      console.debug("Session expired...")
      setSessionResponded(true)
      setLoading(false)
      setSessionExpired(true)
      setActiveWebId(undefined)
    })

    session.onLogin(() => {
      console.debug("Logged in...")
      setSessionResponded(true)
      setLoading(false)
    })

    session.onSessionRestore(() => {
      console.debug("Session restored...")
      setSessionResponded(true)
      setLoading(false)
    })

    session.handleIncomingRedirect().then((sessionInfo?: ISessionInfo) => {
      if (sessionInfo?.isLoggedIn) {
        setActiveWebId(sessionInfo.webId)
      } else {
        setTimeout(() => {
          if (!sessionExpired && !sessionResponded && (auto || manualTrigger)) {
            session
              .handleIncomingRedirect({ restorePreviousSession: true })
              .then((sessionInfo?: ISessionInfo) => {
                if (sessionInfo?.isLoggedIn) {
                  setActiveWebId(sessionInfo.webId)
                } else {
                  setClearInitialLoad(
                    setTimeout(() => {
                      setLoading(false)
                    }, 1000)
                  )
                }
              })
          } else {
            setLoading(false)
          }
        }, 2000)
      }
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [auto, manualTrigger])

  useEffect(() => {
    const clientAuth = JSON.parse(
      localStorage.getItem(
        `solidClientAuthenticationUser:${session.info.sessionId}`
      ) as string
    )
    if (clientAuth?.issuer) {
      const alreadyExists = prevIdps.includes(clientAuth.issuer)
      if (!alreadyExists) {
        setShowRememberPromptFor(clientAuth.issuer)
      }
    }
  }, [prevIdps, session.info.sessionId])

  const connectCallBack = useCallback(() => {
    setManualTrigger(true)
  }, [])

  const abortCallBack = useCallback(() => {
    setManualTrigger(false)
  }, [])

  if (!auto && !manualTrigger && !showRememberPromptFor) {
    return (
      <PodConnectorContext.Provider
        value={{
          connect: connectCallBack,
          abort: abortCallBack,
          restoreState: () => ({ clearState: () => {} }),
        }}
      >
        {children}
      </PodConnectorContext.Provider>
    )
  }

  if (loading) {
    return (
      <div className={styles.wrapper}>
        <div
          className={styles.container}
          style={{
            position: "absolute",
            transform: "translate(-50%, -50%)",
            top: "50%",
            left: "50%",
            padding: "initial",
          }}
        >
          {loadingIndicator ?? "Loading..."}
        </div>
      </div>
    )
  }

  if (activeWebId && showRememberPromptFor && newLogin) {
    return (
      <div className={styles.wrapper}>
        <div
          className={styles.container}
          style={{
            position: "absolute",
            transform: "translate(-50%, -50%)",
            top: "50%",
            left: "50%",
            padding: "initial",
          }}
        >
          <form className={styles.main}>
            <span className={styles.lead}>
              Do you want to save <b>{new URL(showRememberPromptFor).host}</b>{" "}
              as an identity provider for future logins?
            </span>
            <button
              className={styles.remember}
              onClick={(e) => {
                e.preventDefault()
                const newIdps = [...prevIdps, showRememberPromptFor]
                setPrevIdps(newIdps)
                localStorage.setItem("prevIdps", JSON.stringify(newIdps))
                setNewLogin(false)
              }}
            >
              Save Login Info
            </button>
            <button
              className={styles.noRemember}
              onClick={(e) => {
                localStorage.removeItem("newLogin")
                setNewLogin(false)
              }}
            >
              No, thanks
            </button>
          </form>
        </div>
      </div>
    )
  }

  if (!activeWebId) {
    return (
      <div className={classNames(styles.wrapper)}>
        <div className={styles.container}>
          {logo ? <div className={styles.logo}>{logo}</div> : null}
          <h2>{name ?? "Solid Login"}</h2>
          <Helmet>
            <title>{name ? `${name} | Login` : "Login"}</title>
          </Helmet>
          <Formik
            initialValues={{ login: "" }}
            onSubmit={(values) => {
              const oidcIssuer = values.login.startsWith("https://")
                ? values.login
                : `https://${values.login}`
              if (!prevIdps.includes(values.login)) {
                localStorage.setItem("newLogin", "true")
              } else {
                localStorage.removeItem("newLogin")
              }
              session
                .login({ oidcIssuer: oidcIssuer, ...loginOptions })
                .catch((e) => {
                  setInvalidIDP(true)
                })
            }}
          >
            {({ values, handleSubmit, handleChange, setFieldValue }) => {
              const submitCallback = (idp?: string) => {
                setFieldValue("login", idp ?? values.login)
                handleSubmit()
              }
              return (
                <div className={styles.main}>
                  {lead ? (
                    lead
                  ) : (
                    <span>
                      Choose an Identity Provider for this{" "}
                      <a href="https://solidproject.org/">Solid Application</a>
                    </span>
                  )}
                  {!prevIdps.length && recommendedLogins.length ? (
                    <b className={styles.banner}>RECOMMENDED LOGINS</b>
                  ) : null}
                  {prevIdps.length === 0
                    ? recommendedLogins.map((idp) => (
                        <button
                          key={idp}
                          className={styles.prevIdp}
                          onClick={(e) => {
                            e.preventDefault()
                            submitCallback(idp)
                          }}
                        >
                          {idp.replace("https://", "")}
                        </button>
                      ))
                    : null}
                  {prevIdps.length ? (
                    <div className={styles.prevIdpHeader}>
                      <b className={styles.banner}>FROM PREVIOUS LOGINS</b>
                      {prevIdps && (
                        <button
                          className={styles.clear}
                          onClick={(e) => {
                            e.preventDefault()
                            if (
                              // eslint-disable-next-line no-restricted-globals
                              confirm(
                                `Do you really want to clear all your login info for ${
                                  name ?? "this Solid Application"
                                }?`
                              )
                            ) {
                              localStorage.removeItem("prevIdps")
                              setPrevIdps([])
                            }
                          }}
                        >
                          CLEAR
                        </button>
                      )}
                    </div>
                  ) : null}
                  {prevIdps
                    ? prevIdps.map((idp) => (
                        <button
                          key={idp}
                          className={styles.prevIdp}
                          onClick={(e) => {
                            e.preventDefault()
                            submitCallback(idp)
                          }}
                        >
                          {new URL(idp).host}
                        </button>
                      ))
                    : null}
                  {prevIdps.length || recommendedLogins.length ? (
                    <b className={styles.banner}>NEW LOGIN</b>
                  ) : null}
                  <form
                    onChange={() => setInvalidIDP(false)}
                    onSubmit={() => submitCallback(values.login)}
                  >
                    <div className={styles.input}>
                      <input
                        name="login"
                        type="text"
                        placeholder="e.g. inrupt.net"
                        onChange={handleChange}
                      />
                      <button
                        key="submit"
                        type="submit"
                        onClick={(e) => {
                          e.preventDefault()
                          submitCallback()
                        }}
                      >
                        ＋
                      </button>
                    </div>
                    {invalidIDP ? (
                      <span className={styles.error}>
                        Please provide a correct URL.
                      </span>
                    ) : null}
                  </form>
                </div>
              )
            }}
          </Formik>
        </div>
      </div>
    )
  }

  return (
    <PodConnectorContext.Provider
      value={{
        session,
        restoreState: () => {
          const loginFrom = localStorage.getItem("loginFrom") ?? undefined
          return {
            from: loginFrom,
            clearState: () => {
              localStorage.removeItem("loginFrom")
            },
          }
        },
        connect: () => {},
        abort: () => {},
      }}
    >
      {children}
    </PodConnectorContext.Provider>
  )
}

export default PodConnector
