# React Pod Connector

A simple login (with Solid) flow for React applications.

[Demo](https://ludwigschubi.gitlab.io/react-pod-connector-demo/)

✅ Recommended & Custom Identity Provider Logins <br>
✅ Customizable app logo and name <br>
✅ Can be triggered manually (per-page) or automatically <br>
✅ Allows users to save login info for future logins <br>
✅ Dark Mode (lol)

[Overview]() | [Usage]() | [Reference]()

# Overview

This library is designed for you to save time when you start building a new [Solid](https://solidproject.org) application in React.

In it's default configuation it will show a Solid logo and give popular options for Identity Providers like Inrupt's ESS or the Solid Community's NSS, as well as the option to enter a custom IDP login URL (with or without `https://`).

<figure>
<img alt="Desktop Screenshot" src="docs/Screen%20Shot%201.png" />
<figcaption>Desktop</figcaption>
</figure>
<figure>
<img alt="Desktop Screenshot" src="docs/Screen%20Shot%202.png" width="20%" />
<figcaption>Mobile</figcaption>
</figure>

The component also allows users to easily save login info for the future.

<figure>
<img alt="Desktop Screenshot" src="docs/Screen%20Shot%203.png" />
<figcaption>After a new successful login</figcaption>
</figure>

# Usage

Use `yarn add react-pod-connector` or `npm install react-pod-connector` to install.

You can wrap your entire app in this component to always automatically connect users with their pods:

```typescript
// App.tsx
import { PodConnector } from "react-pod-connector"

function App() {
  return (
    <PodConnector>
      <div>Hello World</div>
    </PodConnector>
  )
}
```

Or you can manually trigger the login flow on private pages by adding a route "middleware" to your react router configuration:

```typescript
// PrivateRoute.tsx
import { useConnect } from "react-pod-connector"

interface PrivateRouteProps {
  page: JSX.Element
}

export const PrivateRoute: React.FC<PrivateRouteProps> = ({ page }) => {
  const { session } = useConnect()

  if (session) {
    return page
  } else {
    return <></>
  }
}
```

```typescript
// App.tsx
import PrivateRoute from "./PrivateRoute"

const routes = [
  {
    path: "/",
    element: <div>Hello World! This is a public page</div>,
  },
  {
    path: "/private",
    element: (
      <PrivateRoute page={<div>Hello World! This is a private page</div>} />
    ),
  },
]

function App() {
  const router = createBrowserRouter(routes)

  return (
    <PodConnector auto={false}>
      <RouterProvider router={router} />
    </PodConnector>
  )
}
```

# Reference

These are all the customizable props and their defaults:
| name | default | description |
| ------ | ------ | ------ |
| auto | `true` | Determines whether the component automatically logs in users or waits for the flow to be triggered |
| name | Solid Login | The display name of the application |
| logo | [Logo of the Solid project](https://solidproject.org/assets/img/solid-emblem.svg) | The logo that is displayed |
| lead | Choose an identity provider for this Solid application | The text that is displayed on top of the login form |
| loadingIndicator | - | The loading indicator that is shown when the page first loads or when a session is being restored. |
| recommendedLogins | `["https://login.inrupt.com", "https://solidcommunity.net"]` | A list of login options that are presented to the user as "recommended" |
| loginOptions | `null` | The login options with which to call the login function ([see Inrupt's documentation](https://docs.inrupt.com/developer-tools/api/javascript/solid-client-authn-browser/functions.html#login)). |
